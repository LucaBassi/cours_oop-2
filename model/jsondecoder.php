<?php


class jsondecoder
{
    protected $data;

    /**
     * @return mixed
     */
    public function getData()
    {
        $this->data = json_decode(file_get_contents('model/test.json'), true);
        $this->data;
        return $this->data;
    }

    public function __toString()
    {
        return $this->getData();

    }
}
